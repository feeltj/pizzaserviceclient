export const state = () => ({
	errors: {}
})
/**
 * Return state errors
 */
export const getters = () => ({
	errors(state) {
		return state.errors
	}
})
/**
 * Set Validation error
 */
export const mutations = {
	SET_VALIDAION_ERRORS (state, errors) {
       state.errors = errors
	}
}
/**
 * We can show erors and clear errors when we will finish
 */
export const actions = {
	setErrors({ commit }, errors) {
		commit('SET_VALIDAION_ERRORS', errors)
	},
	clearErrors({commit})
	{
		commit('SET_VALIDAION_ERRORS', {})
	}
}