/**
 * Show categories with empty array
 */
export const state = () => ({
    categories : []
});
/**
 * In mutations we are set categories
 */
export const mutations = {
    SET_CATEGORIES(state, items){
        state.categories = items
    }
}
/**
 * We get categories and can show in components
 */
export const getters = {
	categories(state) {
       return state.categories
	}
}
/**
 * We are get categories from API in Laravel
 */
export const actions = {
    async nuxtServerInit ({commit, dispatch}){
        const response = await this.$axios.$get('categories')
        commit('SET_CATEGORIES', response.data)

        if(this.$auth.loggedIn){
            await dispatch('cart/getCart')
        }
    }
}
